from nltk.tokenize import RegexpTokenizer
import nltk
from gensim.models import Word2Vec
from sklearn.feature_extraction.text import TfidfVectorizer,CountVectorizer
from sklearn.preprocessing import scale
from tqdm import tqdm, trange
from nltk.corpus import stopwords
from multiprocessing import Pool
import pandas as pd
import numpy as np
import re
nltk.download('punkt')
nltk.download('stopwords')

WORD_2_VEC_SIZE = 100
NUM_PARTITIONS = 12 #number of partitions to split dataframe
NUM_CORES = 4 #number of cores on your machine


def parallelize_dataframe(df, func):
    df_split = np.array_split(df, NUM_PARTITIONS)
    pool = Pool(NUM_CORES)
    df = pd.concat(pool.map(func, df_split))
    pool.close()
    pool.join()
    return df

def get_sentences(text):
    """From a text get the sentences"""
    # Cleaing the text
    processed_text = text.lower()
    processed_text = re.sub('[^a-zA-Z]', ' ', processed_text)
    processed_text = re.sub(r'\s+', ' ', processed_text)

    # Preparing the dataset
    return nltk.sent_tokenize(processed_text)


def get_words(sentences):
    """From a list of sentences get the words of each sentence, the return will be a list of lists with the words"""
    all_words = [nltk.word_tokenize(sent) for sent in sentences]
    # Removing Stop Words
    for i in trange(len(all_words)):
        all_words[i] = [w for w in all_words[i] if w not in stopwords.words('english')]
    return all_words

def get_words_from_string(string):
    """From a string get the words, the return will be a list of words"""
    processed_text = string.lower()
    processed_text = re.sub('[^a-zA-Z]', ' ', processed_text)
    processed_text = re.sub(r'\s+', ' ', processed_text)
    all_words = nltk.word_tokenize(processed_text)
    # Removing Stop Words
    all_words = [w for w in all_words if w not in stopwords.words('english')]
    return all_words


def get_word2vec_model_from_words(all_words):
    """Generate the model from a list of words from sentences"""
    return Word2Vec(all_words, min_count=10, size=WORD_2_VEC_SIZE)

def get_words_from_corpus(corpus):
    """From a corpus with multiple texts separate in sentences and after that transform each sentence in a list of words"""
    sentences = []
    for text in tqdm(corpus):
        sentences += get_sentences(text)
    all_words = get_words(sentences)
    return all_words

def get_word2vec_model(corpus):
    """From a corpus create a word2vec model"""
    all_words = get_words_from_corpus(corpus)
    return get_word2vec_model_from_words(all_words)


def convert_text_to_vecs(comment, model):
    """Convert a text to sentences and after that convert each word of each sentence to a vector"""
    sentences = []
    for text in tqdm(corpus):
        sentences += get_sentences(text)
    sentences = []
    sentences += get_words(get_sentences(comment))
    all_words = get_words(sentences)
    return list(map(lambda x: get_vec(x, model), all_words))


def build_tf_idf_dict(list_of_words):
    """Create a dict with the idf of each word from a list of list of words"""
    vectorizer = TfidfVectorizer(analyzer=lambda x: x, min_df=10)
    matrix = vectorizer.fit_transform(list_of_words)
    idf = dict(zip(vectorizer.get_feature_names(), vectorizer.idf_))
    return idf

def build_tf_idf_matrix(list_of_comments, use_tf=True):
    """Create a sparse Matrix with the tf-idf of each word from a list of list of words"""
    if use_tf:
        vectorizer = TfidfVectorizer(ngram_range = (1,3), max_df=0.3, min_df=10,use_idf=False)
    else:
        vectorizer = CountVectorizer(ngram_range = (1,3), max_df=0.3, min_df=10)
    matrix = vectorizer.fit_transform(list_of_comments)
    return matrix, vectorizer


def build_tf_idf_dict_from_serie(serie):
    """Create a dict with the idf of each word from a pandas series with strings"""
    list_of_words = get_words(serie.tolist())
    idf = build_tf_idf_dict(list_of_words)
    return idf

def build_tf_idf_matrix_from_serie(serie, use_tf = True):
    """Create a tf-idf sparse_matrix from a pandas series with strings"""
    matrix, model = build_tf_idf_matrix(serie.tolist(), use_tf)
    return matrix, model

def count_uper(string):
    return sum(1 for c in string if c.isupper())/len(string)

def count_specials_chars(string):
    return len( re.findall('[^\w-]', string) )/len(string)

def count_words_repeated_in_sequence(string):
    words = string.split()
    actual_words=None
    repeated_max=1
    repeated=1
    for word in words:
        if actual_words==word:
            repeated+=1
            if repeated_max<repeated:
                repeated_max=repeated
        else:
            actual_words=word
            repeated=1
    return repeated_max


class Word2VecConvert:
    
    def __init__(self, model,idf,TrainPhaseDebug=False):
        self.model = model
        self.idf = idf
        self.TrainPhaseDebug = TrainPhaseDebug
    
    def _build_vector_from_comments(self, comment, use_idf  = True):
        """Create a vector representation of a vector summing the vector the vector representation of each word
        weighting with the idf in the corpus of this word"""
        all_words = get_words_from_string(comment)
        vec = np.zeros(WORD_2_VEC_SIZE).reshape((1, -1))
        count = 0.
        for word in all_words:
            try:
                if use_idf:
                    idf_value = self.idf[word]
                else: 
                    idf_value = 1
                vec += self.model.wv[word].reshape((1, -1)) * idf_value
                # Maybe 1. would be a better value, test is needed. [WIP]
                count += idf_value
            except KeyError:  # handling the case where the token is not in the corpus. useful for testing.
                if(self.TrainPhaseDebug):
                    print("Word {} not in corpus".format(word))
                continue
        if count != 0:
            vec /= count
        return vec.ravel()
    
    def map_vectors(self, serie):
        result = serie.map(self._build_vector_from_comments)
        return result
    
    def transform_series_text_in_vector(self, serie, use_idf = True):
        """transform a panda series with strings in the vector representation of that string using a Word2VecModel and idf dict
        if in TrainPhase is expected to all the words be in the corpus so if TrainPhaseDebug is set True a error will rise if a word it's not in
        the corpus"""
        if use_idf:
            vector_representation_dataframe = pd.DataFrame(columns=range(WORD_2_VEC_SIZE))
            count = 0
            for list_of_words in parallelize_dataframe(serie,self.map_vectors):
                vector_representation_dataframe.loc[count] = list_of_words
                count += 1
            return vector_representation_dataframe
        else:
            vector_representation_dataframe = pd.DataFrame(columns=range(WORD_2_VEC_SIZE))
            count = 0
            for comment in serie:
                vector_representation_dataframe.loc[count] = self._build_vector_from_comments(comment, use_idf)
                count += 1
            return vector_representation_dataframe   
    def get_vec(self, word):
        """Get the vector representation of a word from a word2vecmodel"""
        return self.model.wv[word]